+++
title = "How to ignore changes for files that are already tracked in Git"
date = "2023-05-28T18:35:00+08:00"
categories = ["Git"]
summary = "It's simple, just use the parameter `--assume-unchange`. "
+++

### How to do

It's simple, we can use the following commands:

```bash
git update-index --assume-unchanged src/example.js
```

If we want to watch the file changes again, we can use the following commands:

```bash
git update-index --no-assume-unchanged src/example.js
```

How to list the files that have been assumed unchanged:

```shell
git ls-files -v | grep '^h'
```

If you use Powerbash, use this:

```shell
git ls-files -v | Select-String -CaseSensitive '^h'
```

### References

* [Ignore files in Git without adding them to .gitignore | Luis Dalmolin](https://luisdalmolin.dev/blog/ignoring-files-in-git-without-gitignore/)
* [version control - Undo git update-index --assume-unchanged <file> - Stack Overflow](https://stackoverflow.com/questions/17195861/undo-git-update-index-assume-unchanged-file)
