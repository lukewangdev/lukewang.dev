+++
title = "To be effective"
date = "2023-05-11T15:03:50+08:00"
categories = ["Life"]
summary = "Take back control of my life."
+++

## Updated on June 15, 2024

After one year, I feel lost and a little awkward when I read this post. To be honest, no one thing is finished perfectly.

It's so hard to lose weight, which is still stuck at 100 kilograms. Learning computer networking and TypeScript hasn't even started. It's not so bad that I got a rudimentary IELTS score.

I have to face this embarrassed situation, my study journey to Canada may break down because of long-time background checking for study permit application, although I have deferred the entry date last year. The uncertain situation made me so nervous in the last few months. But it taught another lesson, which is about keeping the choices in my own hands. I don't know what will happen in the future, but I would keep learning and growing in this not good world.

So, I am back. I will update my blog weekly or biweekly to remind myself that keeping learning is the only way I can maintain control over my choices.

Nice.

## May 11, 2023

### Monthly Goals

- [ ] Lose Weight: 102.1 kg -> 97 kg
- [ ] Computer Networking
- [ ] TypeScript beginner level
- [ ] IELTS: 7, 7, 6.5, 6.5

### 10th May - 15th May

- [x] Write blogs to record my study progress
- [ ] Lose Weight to 100 kg, Daily Caloric Expenditure 1000 KCal
- [ ] IELTS: finish 5 Cambridge Mock Testes
- [ ] Computer Networking: Chapter 1
- [ ] TypeScript: Get Started
    * (11th May) I need to co-learn JavaScript
