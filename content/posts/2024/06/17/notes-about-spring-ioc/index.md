+++
title = "Notes about Spring IoC"
date = "2024-06-17T20:52:40+08:00"
categories = ["Java", "Spring"]
summary = "My notes about Spring IoC."
+++

* The `org.springframework.beans` and `org.springframework.context` packages are the basis for Spring Framework's IoC container.
* `BeanFactory` provides the configuration framework and basic functionality, and the `ApplicationContext` adds more enterprise-specific functionality.

```goat
+--------------------------+                                                            
|                          |                                                            
|        interface         |                                                            
|                          |       - provides configuration mechanism capable to        
|       BeanFactory        |         managing any type of object                        
|                          |                                                            
+-----------^--------------+                                                            
            |                                                                           
            |                                                                           
+-----------+--------------+                                                            
|                          |       adds features:                                       
|        interface         |       - easier intergration with Spring's AOP features     
|                          |       - message resource handling (internationalization)   
|    ApplicationContext    |       - event publication                                  
|                          |       - application-layer specific contexts such as the    
+--------------------------+         `WebApplicationContext` for use in web applications
```

* What are beans in Spring?
    > In Spring, the objects that form the backbone of your application and that are managed by the Spring IoC container are called beans. A bean is an object that is **instantiated, assembled, and managed** by a Spring IoC container. Otherwise, a bean is simply one of many objects in your application. Beans, and the dependecies among them, are **reflected** in the **configuration metadata** used by a container.
* AOP [Reference](https://www.linkedin.com/pulse/aspect-oriented-programming-forbabies-jonathan-manera/)
    > A `Pointcut` is a predicate that matches a `Join Point`. `Advice` is associated with a `Pointcut` and runs at any `Join Point` matched by the `Pointcut`.
    ![AOP](./assets/aop.png)