+++
title = "Common Post Body Types in HTTP"
date = "2024-06-27T20:11:52+08:00"
categories = ["HTTP"]
summary = "These are the common types used in HTTP POST requests, each serving different purposes and use cases."
+++

In the context of HTTP (Hypertext Transfer Protocol), "post body types" generally refer to the different ways data can be formatted and sent in the body of a POST request. Here are some common types:

### 1. `application/x-www-form-urlencoded`
- **Format**: This is the default encoding type for HTML forms. Data is encoded as key-value pairs, with special characters replaced by percent-encoded values.
- **Example**:
  ```
  name=Luke+Wang&age=28
  ```

### 2. `multipart/form-data`
- **Format**: This encoding type is used when uploading files. Each part of the multipart message can contain a different type of data.
- **Example**:
  ```
  --boundary
  Content-Disposition: form-data; name="field1"

  value1
  --boundary
  Content-Disposition: form-data; name="file"; filename="example.txt"
  Content-Type: text/plain

  (file content)
  --boundary--
  ```

### 3. `application/json`
- **Format**: Data is formatted as JSON (JavaScript Object Notation), which is a lightweight data interchange format.
- **Example**:
  ```json
  {
    "name": "Luke Wang",
    "age": 28
  }
  ```

### 4. `text/plain`
- **Format**: Data is sent as plain text, without any specific encoding. This is less common and typically used for simpler data exchanges.
- **Example**:
  ```
  name=Luke Wang&age=28
  ```

### 5. `application/xml`
- **Format**: Data is formatted as XML (Extensible Markup Language), which is a markup language that defines a set of rules for encoding documents.
- **Example**:
  ```xml
  <person>
    <name>Luke Wang</name>
    <age>28</age>
  </person>
  ```

### 6. `application/octet-stream`
- **Format**: This is used for binary data. It indicates that the data is not text and can be any arbitrary binary stream.
- **Example**: Sending a binary file as the post body.

### 7. `application/graphql`
- **Format**: This is used for sending GraphQL queries in the body of the request.
- **Example**:
  ```graphql
  {
    user(id: "1") {
      name
      age
    }
  }
  ```

These are the common types used in HTTP POST requests, each serving different purposes and use cases.
