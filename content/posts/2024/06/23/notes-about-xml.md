+++
title = "Notes about XML"
date = "2024-06-23T15:46:37+08:00"
categories = ["Java", "Spring"]
summary = "Simple notes about XML."
+++

## Exmaple

```xml
<?xml version="1.0" encoding="UTF-8" ?> 
⬆️ XML file header

<web-app xmlns="http://java.sun.com/xml/ns/javaee"
            ⬆️ Define the URI as the default namespace,
               which means all child elements without prefix belongs to it

         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            ⬆️ Define a prefix called `xsi`

         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                             http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
            ⬆️ Define a property `schemaLocation` to `xsi`
               1. first line is the namespace URI, which is a unique identifier
               2. second line points to the physical location of an XSD file,
                  detailing the structure and constrains that the XML doucument must follow
               3. it is crucial that the namespace URIs and their corresponding
                  XML Schema location URIs are defined in pairs and in order
         version="3.0">
</web-app>
```

## Description

* if there is a `"http://www.w3.org/2001/XMLSchema-instance"`, XML parser would try to find `xsi:schemaLocation` to check xml document whether well formatted
* XML Schema: Structures also defines several attributes for direct use in any XML documents. These attributes are in a different namespace, which has the namespace name `http://www.w3.org/2001/XMLSchema-instance`. For brevity, the text and examples in this specification use the prefix `xsi:` to stand for this latter namespace; **in practice, any prefix can be used**. [Reference](https://www.w3.org/TR/xmlschema-1/#Instance_Document_Constructions)
* In theory, it should work properly.
    ```xml
    <?xml version="1.0" encoding="UTF-8" ?>
    <web-app xmlns="http://java.sun.com/xml/ns/javaee"
            xmlns:😊="http://www.w3.org/2001/XMLSchema-instance"
            😊:schemaLocation="http://java.sun.com/xml/ns/javaee
                               http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
            version="3.0">
    </web-app>
    ```
* Another complex example: https://github.com/hiwangzi/learn-spring-in-action-v4/blob/fd4ea6d1eef81a3922626c2566e28dd5e7623b06/chapter1/src/main/resources/knights.xml
